# README
## Release 0.8.14A
  - Restart the project 2021-11-01
## Release 0.8.12
  - Adjust MGRS box size.
## Release 0.8.10
- Update spec to change the length of the "Commit date" in Application_helper
- Fixed issue with pre-commit hook (Husky 🐶!!)
## Release 0.8.9
New specs for Application Helper.

Release 0.8.4 + Hotfix
Release 0.8.8 
## Status
[![build status](https://gitlab.com/ycrepeau/test-vue3/badges/master/pipeline.svg)](https://gitlab.com/ycrepeau/test-vue3/badges/master)

[![Coverage report](https://gitlab.com/ycrepeau/test-vue3/badges/master/coverage.svg)](http://ycrepeau.gitlab.io/test-vue3/coverage/)



## More to come

(C) Copyright Yanik Crépeau 2021

FROM ruby:2.7.2-alpine AS build-env
#FROM ruby:3.0.1-alpine3.13 AS build-env

# Set an environment variable where the Rails app is installed to inside of Docker image
ENV RAILS_ROOT /var/www/app_name

# Setting env up
ENV RAILS_ENV='production'
ENV RACK_ENV='production'

ENV BUILD_PACKAGES="build-base curl-dev git"
ENV DEV_PACKAGES="postgresql-dev yaml-dev zlib-dev nodejs "
ENV RUBY_PACKAGES="tzdata"

ENV BUNDLE_APP_CONFIG="$RAILS_ROOT/.bundle"

#
# Create and set working directory
RUN mkdir -p $RAILS_ROOT
WORKDIR $RAILS_ROOT

# Install bundler
RUN gem install bundler -v 2.2.30

# hello!
# install packages
RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $BUILD_PACKAGES $DEV_PACKAGES $RUBY_PACKAGES \
    && apk add --no-cache yarn --repository="http://dl-cdn.alpinelinux.org/alpine/edge/community" \
    && yarn -v
    
COPY Gemfile* package.json yarn.lock ./

# install rubygem
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock

RUN bundle config --global frozen 1 \
    && bundle install --without development:test:assets -j4 --retry 3 --path=vendor/bundle \
    # Remove unneeded files (cached *.gem, *.o, *.c)
    && rm -rf vendor/bundle/ruby/2.7.0/cache/*.gem \
    && find vendor/bundle/ruby/2.7.0/gems/ -name "*.c" -delete \
    && find vendor/bundle/ruby/2.7.0/gems/ -name "*.o" -delete
    
RUN yarn install --production

COPY . .
RUN bin/rails webpacker:compile
RUN bin/rails assets:precompile
# Remove folders not needed in resulting image
#RUN rm -rf node_modules vendor/assets spec tmp/cache/assets  tmp/cache/bootsnap-compile-cache tmp/cache/pids
# tmp/cache


############### Build step done ###############
FROM ruby:2.7.2-alpine
#FROM ruby:3.0.1-alpine3.13
ENV RAILS_ROOT /var/www/app_name
ARG PACKAGES="tzdata postgresql-client nodejs bash"

# Setting env up
ENV RAILS_ENV='production'
ENV RACK_ENV='production'
ENV BUNDLE_APP_CONFIG="$RAILS_ROOT/.bundle"
WORKDIR $RAILS_ROOT

# Install bundler
RUN gem install bundler -v 2.2.30

# install packages
RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $PACKAGES
    
COPY --from=build-env $RAILS_ROOT $RAILS_ROOT
EXPOSE 3000
#CMD ["bin/rails", "server", "-b", "0.0.0.0"]
CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
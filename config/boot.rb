ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)

require 'bundler/setup' # Set up gems listed in the Gemfile.
require 'bootsnap/setup' # Speed up boot time by caching expensive operations.

if ENV['HEROKU_RELEASE_VERSION']
  $deploy_version = 'heroku ' + ENV['HEROKU_RELEASE_VERSION'] + ' ' + ENV['HEROKU_RELEASE_CREATED_AT']
elsif ENV['XBUILD_TIMESTAMP']
  $deploy_version = 'docker ' + ENV['XBUILD_TIMESTAMP']
else
  $deploy_version = 'local'
end
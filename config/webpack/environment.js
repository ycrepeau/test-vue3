const { environment } = require('@rails/webpacker')
const { VueLoaderPlugin } = require('vue-loader')
const vue = require('./loaders/vue')
const coffee = require('./loaders/coffee')
const sass =  require('./loaders/sass')
const scss =  require('./loaders/scss')
const haml =  require('./loaders/haml')

environment.loaders.prepend('vue', vue)
environment.loaders.prepend('sass', sass)
environment.loaders.prepend('scss', scss)
environment.loaders.prepend('haml', haml)
environment.loaders.prepend('coffee', coffee)
environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin())

module.exports = environment

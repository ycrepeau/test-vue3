module.exports = {
  test: /\.scss(\.erb)?$/,
  use: [
    'style-loader',
    'css-loader',
    {
      loader: 'sass-loader',
      options: {
        sassOptions: {
          indentedSyntax: false
        }
      }
    }
  ]
}
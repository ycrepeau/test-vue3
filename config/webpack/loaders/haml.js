module.exports = {
  test: /\.haml(\.erb)?$/,
  use: [{
    loader: 'haml-plain-loader'
  }]
}
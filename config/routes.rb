Rails.application.routes.draw do
  get 'other/index'
  get 'bonjour/index'
  root 'bonjour#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  ## The next route is used to support webpack and vue-router
  get '/*path', to: 'bonjour#index', format: false

end

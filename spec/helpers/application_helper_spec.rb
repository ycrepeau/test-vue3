require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the OtherHelper. For example:
#
# describe OtherHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ApplicationHelper, type: :helper do
  # pending "add some examples to (or delete) #{__FILE__}"
  describe "#embeded_info" do
    it "is not null" do
      expect(embeded_info).not_to be_empty
    end

    it "contains data" do
      expect(embeded_info.length).to be >= 35
    end

    it "contains a date" do
      expect(embeded_info).to match(/^Commit date: \d{4}-\d{2}-\d{2}_\d{2}:\d{2}:\d{2} UTC ----$/)
    end
  end
end

require 'rails_helper'

RSpec.describe "ApplicationRecord", type: :model do
  describe "class abstraction? " do
    it "is abstract" do
      expect(ApplicationRecord.abstract_class?).to be true
    end
  end
end
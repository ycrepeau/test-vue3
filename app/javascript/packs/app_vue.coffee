import "foundation"

import Rails from "@rails/ujs"
import TurbolinksAdapter from "vue-turbolinks"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import Vue from 'vue/dist/vue.esm'
import VueRouter from 'vue-router'
import App from '../vue/app.vue'
import { $$, router } from '../common'
import $ from 'jquery'

####
Vue.use(TurbolinksAdapter)
Vue.use(VueRouter)

Rails.start()
Turbolinks.start()
ActiveStorage.start()

$$ 'turbolinks:render', ->
  console.log 'turbolinks:render **'

$$ 'turbolinks:before-visit', ->
  console.log 'turbolinks:before-visit'

$$ 'turbolinks:before-render', (evt) ->
  console.log 'turbolinks:before-render'
  #console.log evt.data.newBody.innerText

setup = ->
  if $('#webpack_entry_point').length
    console.log '#webpack_entry_point present creaing Vue'
    app_vue = new Vue {
      router
      el: '#webpack_entry_point'
      template: '<App/>'
      data: {
        message: "Pourrais-tu dire Allo?"
      }
      components: {
        App
      }
    }
  else
    console.log '#webpack_entry_point NOT PRESENT'

   $(document).foundation()

$ ->
  console.log('Page did finish loading')

$$ -> #
  
  console.log 'turbolinks:load (app-vue.coffee)'
  setup()

###
beforeCreate: ->
  console.log 'root before create'

created: ->
  console.log 'root created'

beforeMount: ->
  console.log 'root before mount'

mounted: ->
  console.log 'root mounted'

beforeUpdate: ->
  console.log 'root before update'

updated: ->
  console.log 'root updated'

beforeDestroy: ->
  console.log 'root before destroy'

destroyed: ->
  console.log 'root destroyed'

###






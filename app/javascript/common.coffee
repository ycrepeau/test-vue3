import VueRouter from 'vue-router'
import Carte from './vue/carte.vue'
import Legend from './vue/legend.vue'
import Home from './vue/home.vue'

#
# $$ - eventListener binder.
# use:
# only on function in param: bind turbolinks:load to document.
# 1 param + function: bind the event provided to document
# 2 params + function: bind event (2nd arg) to target (1st arg)
#
$$ = (args..., func) ->
  if args.length == 0
    target = window.document
    binding = 'turbolinks:load'
  if args.length == 1
    target = window.document
    binding = args[0]
  if args >= 2
    target = args[0]
    binding = args[1]

  target.addEventListener binding , func

router = new VueRouter ({
  mode: 'history',
  routes: [
    { path: '/',       component: Home, name: 'home' }
    { path: '/carte',  component: Carte, name: 'carte' }
    { path: '/legend', component: Legend, name: 'legend' }
  ]
})

export {
  $$,
  router
}

# Fin du fichier